<?php

namespace DefStudio\Logger;
include_once 'Manager.php';


if(isset($_POST['request'])){
    $request = $_POST['request'];
}else if(isset($argv[1])){
    $request = $argv[1];
}



$manager = new Manager($request);


$response = $manager->process();


die(json_encode($response));

