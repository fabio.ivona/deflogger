<?php
/**
 * Created by PhpStorm.
 * User: zioba
 * Date: 03/12/2018
 * Time: 11:12
 */

namespace DefStudio\Logger;

include_once dirname(__FILE__) . "/../../../autoload.php";

use Firebase\JWT\JWT;

class Manager {
    const STATUS_CODE_ERROR = 0;
    const STATUS_CODE_OK = 1;

    private $request;

    public function __construct($request = null){
        $this->request = $request;
    }

    public function process(){


        if(empty($this->request)) {
            return $this->error('No Valid Data');
        }

        try {
            $token = $this->decode_jwt($this->request);


            $data = $token->data;


            if(empty($data)) return $this->error('Processing error');


            if(empty($data->api)) return $this->error('Processing error');


            switch($data->api) {
                case 'get_mysql_logs':
                    return $this->get_mysql_logs($data);
                    break;
                default:
                    return $this->error('No valid API');
                    break;
            }

        } catch(\Exception $ex) {
            return $this->error('Processing error');
        }
    }


    private function get_mysql_logs($data){
        if(empty($data->from_id)) $data->from_id = -1;

        try {
            $logs = Logger::get_mysql_logs($data->from_id);
        } catch(\Exception $ex) {

            return $this->error($ex->getMessage());
        }
	
		foreach($logs as $index => $row){
			$ok = json_encode($row);
			if(!$ok){
				unset($logs[$index]);
			}
		}

		
        return $this->response(self::STATUS_CODE_OK, 'Ok', ['logs' => $logs]);
    }

    /**
     * Returns an error array response
     * @param $message
     * @return array returns an array containing [status, message]
     */
    private function error($message = ''){
        return $this->response(self::STATUS_CODE_ERROR, $message);
    }

    /**
     * Returns a response array
     * @param $status_code
     * @param $message
     * @param array $data
     * @return array returns an array containing [status, message, data]
     */
    private function response($status_code, $message, $data = []){
        $encoded_data = $this->encode_jwt($data);

        return [
            'status' => $status_code,
            'message' => $message,
            'jwt' => $encoded_data,
        ];
    }

    /**
     * Encode data in a JWT token
     * @param array $data
     * @return null|string returns a JWT token or null if data is not valid
     */
    private function encode_jwt($data = []){
        if(empty($data)) {
            return null;
        }

        $token = [
            "iss" => Logger::get_config('jwt_iss'),
            "data" => $data,
        ];

        return JWT::encode($token, Logger::get_config('jwt_key'), 'HS256');
    }

    /**
     * Decode a jwt token
     * @param $data
     * @return null|object
     */
    private function decode_jwt($data){
        if(empty($data)) {
            return null;
        }


        return JWT::decode($data, Logger::get_config('jwt_key'), ['HS256']);
    }
}