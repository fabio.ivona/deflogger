<?php
/**
 * Created by PhpStorm.
 * User: zioba
 * Date: 30/11/2018
 * Time: 18:04
 */


namespace DefStudio\Logger;

use Exception;
use PDO;
use PDOException;


class Logger{
    const DB_LOG_TABLE = 'def_logger';

    const LEVEL_DEBUG = 'DEBUG';
    const LEVEL_INFO = 'INFO';
    const LEVEL_NOTICE = 'NOTICE';
    const LEVEL_WARNING = 'WARNING';
    const LEVEL_ERROR = 'ERROR';
    const LEVEL_CRITICAL = 'CRITICAL';
    const LEVEL_ALERT = 'ALERT';
    const LEVEL_EMERGENCY = 'EMERGENCY';


    public static function get_config($config_name, ...$cfg){
        include dirname(__FILE__) . "/../../../../def_logger_config.php";

        if(isset($cfg[2])){
            return empty($def_logger_config[$config_name][$cfg[0]][$cfg[1]][$cfg[2]]) ? null : $def_logger_config[$config_name][$cfg[0]][$cfg[1]][$cfg[2]];
        }
        if(isset($cfg[1])){
            return empty($def_logger_config[$config_name][$cfg[0]][$cfg[1]]) ? null : $def_logger_config[$config_name][$cfg[0]][$cfg[1]];
        }
        if(isset($cfg[0])){
            return empty($def_logger_config[$config_name][$cfg[0]]) ? null : $def_logger_config[$config_name][$cfg[0]];
        }
        return empty($def_logger_config[$config_name]) ? null : $def_logger_config[$config_name];
    }

    private static function db(){

        $options = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ];


        $host = self::get_config('connectors', 'mysql', 'db_host');
        $database = self::get_config('connectors', 'mysql', 'db_name');
        $user = self::get_config('connectors', 'mysql', 'db_user');
        $password = self::get_config('connectors', 'mysql', 'db_password');

        try{
            // crea la connessione al database
            $db = new PDO ("mysql:host={$host};dbname={$database};charset=utf8", $user, $password, $options);
        } catch(PDOException $ex){
            // gestione degli errori di connessione
            throw new Exception('DefLogger: Failed to connect to database');
        }

        // imposta il db per generare un'eccezione in caso di errore, possiamo gestire gli errori con un try-catch
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // imposta il db per restituire le righe di risultato in un array associativo, cos   da poter accedere velocemente ai campi
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);


        // disabilita le 'magic quotes'
        self::undo_magic_quotes_gpc($_POST);
        self::undo_magic_quotes_gpc($_GET);
        self::undo_magic_quotes_gpc($_COOKIE);


        //initialize logger table
        try{
            $stmt = $db->prepare("SHOW TABLES LIKE :db_log_table");
            $stmt->execute(['db_log_table' => self::DB_LOG_TABLE]);
        } catch(PDOException $ex){
            throw new Exception('DefLogger: database initialization error');
        }
        if(!$stmt->fetch()){
            try{
                $db_log_table = self::DB_LOG_TABLE;
                $query = " CREATE TABLE $db_log_table (
                                  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                                  timestamp datetime DEFAULT NOW(),
                                  app VARCHAR(128) DEFAULT NULL,
                                  module VARCHAR(128) DEFAULT NULL,
                                  user VARCHAR(64) DEFAULT 'no user',
                                  level VARCHAR(16) DEFAULT NULL,
                                  message VARCHAR(256) DEFAULT NULL,
                                  data MEDIUMBLOB DEFAULT NULL,
                                  file VARCHAR(512) DEFAULT NULL,
                                  line VARCHAR(10) DEFAULT NULL,
                                  file_lines BLOB DEFAULT NULL
                               )ENGINE=InnoDB DEFAULT CHARSET=latin1;";
                $stmt = $db->prepare($query);
                $stmt->execute(['db_log_table' => self::DB_LOG_TABLE]);
            } catch(PDOException $ex){
                throw new Exception('DefLogger: database initialization error');
            }
        }

        return $db;
    }

    private static function undo_magic_quotes_gpc(&$array){
        foreach($array as &$value){
            if(is_array($value)){
                self::undo_magic_quotes_gpc($value);
            } else{
                $value = stripslashes($value);
            }
        }
    }

    private static function level_map($level){
        switch($level){
            case self::LEVEL_DEBUG:
                return 100;
            case self::LEVEL_INFO:
                return 200;
            case self::LEVEL_NOTICE:
                return 250;
            case self::LEVEL_WARNING:
                return 300;
            case self::LEVEL_ERROR:
                return 400;
            case self::LEVEL_CRITICAL:
                return 500;
            case self::LEVEL_ALERT:
                return 550;
            case self::LEVEL_EMERGENCY:
                return 600;
            default:
                return 0;
        }
    }

    /**
     * @param $level
     * @param $message
     * @param $data
     * @param $options
     */
    private static function log($level, $message, $data, $options){
        if(self::level_map($level) >= self::level_map(self::get_config('minimum_log_level'))){

            $stack = debug_backtrace(false);
            $backtrace = '';
            foreach($stack as $stack_entry){
                if(!preg_match('/Logger\.php$/', $stack_entry['file'])){
                    $backtrace = $stack_entry;
                    break;
                }


            }

            //sets default options
            if(empty($options['user'])) $options['user'] = is_callable(self::get_config('get_user_function')) ? call_user_func(self::get_config('get_user_function')) : '';
            if(empty($options['module'])) $options['module'] = self::get_config('module');
            if(empty($options['app'])) $options['app'] = self::get_config('app');
            if(empty($options['file'])) $options['file'] = (empty($backtrace["file"]) ? '' : $backtrace["file"]);
            if(empty($options['line'])) $options['line'] = (empty($backtrace["line"]) ? '' : $backtrace["line"]);


            $data = self::convert_to_array($data);
            $data = json_encode($data);

            //check context
            $context_ok = true;
            if(self::get_config('context')){
                $context_ok = false;
                foreach(self::get_config('context') as $context){
                    if(strpos($options['file'], $context) !== false){
                        $context_ok = true;
                    }
                }
            }

            if($context_ok){

                //salva le righe del file
                $lines = [];
                //TODO: impostare livello minimo di memorizzazione delle rige
                if(self::get_config('save_lines') && self::level_map($level) >= self::level_map(self::get_config('save_lines', 'min_level'))){
                    $lines = self::get_file($options['file'], $options['line'], self::get_config('save_lines', 'before'), self::get_config('save_lines', 'after'));
                }

                if(self::get_config('connectors', 'mysql')){
                    self::log_db($level, $message, $data, $lines, $options);
                }
            }
        }
    }


    private static function convert_to_array($object){
        $result = self::object_to_array_recusive($object);
        return $result;
    }

    private static function object_to_array_recusive($object){
        $result = [];

        if(is_object($object)){
            $array = (array) $object;
            $class = get_class($object);
            foreach($array as $key=>$value){
                $key_split = explode("\0", $key);
                $key = end($key_split);
                $result[$class][$key] = self::object_to_array_recusive($value);
            }
            return $result;
        }else if(is_array($object)){
            $array = $object;
            foreach($array as $key=>$value){
                $result[$key] = self::object_to_array_recusive($value);
            }
            return $result;
        }else{
            return $object;
        }


    }

    private static function get_file($file, $line, $before, $after){
        if(empty($file)) return [];
        if(empty($line)) $line = 0;
        if(empty($before)) $before = 0;
        if(empty($after)) $after = PHP_INT_MAX;

        $from_line = $line - $before;
        $to_line = $line + $after;

        if($from_line < 0) $from_line = 0;

        if($to_line < $from_line) return [];

        if(!file_exists($file)) return [];

        $lines = file($file);

        if($to_line >= count($lines)){
            $to_line = count($lines) - 1;
        }

        $slice = [];

        for($i = $from_line; $i <= $to_line; $i++){
            $slice[$i + 1] = highlight_string($lines[$i], true);
        }

        return $slice;
    }


    /**
     * @param $level
     * @param $message
     * @param $data
     * @param $options
     * @throws Exception
     */
    private static function log_db($level, $message, $data, $lines, $options){
	    $db_log_table = self::DB_LOG_TABLE;
	    $query = " INSERT INTO $db_log_table (app, module, user, level, message, data, file, line, file_lines)
                               VALUES (:app, :module, :user, :level, :message, :data, :file, :line, :file_lines)";
	    $params = [
		    'app'        => $options['app'],
		    'module'     => $options['module'],
		    'user'       => $options['user'],
		    'level'      => $level,
		    'message'    => $message,
		    'data'       => $data,
		    'file'       => $options['file'],
		    'line'       => $options['line'],
		    'file_lines' => json_encode($lines),
	    ];
	
	    try{
		    $db = self::db();
		    $stmt = $db->prepare($query);
		    $stmt->execute($params);
	    } catch(PDOException $ex){
		    unset($params['data']);
		    Logger::emergency("DefLogger: database error", [
			    'Error' => $ex->getMessage(),
			    'data' => $params
		    ]);
	    }
    }


    //<editor-fold desc="PUBLIC METHODS">

    /**
     * Writes a debug entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function debug($message, $data = '', $options = []){
        self::log(self::LEVEL_DEBUG, $message, $data, $options);
    }

    /**
     * Writes an info entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function info($message, $data = '', $options = []){
        self::log(self::LEVEL_INFO, $message, $data, $options);
    }

    /**
     * Writes an notice entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function notice($message, $data = '', $options = []){
        self::log(self::LEVEL_NOTICE, $message, $data, $options);
    }

    /**
     * Writes a warning entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function warning($message, $data = '', $options = []){
        self::log(self::LEVEL_WARNING, $message, $data, $options);
    }

    /**
     * Writes a error entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function error($message, $data = '', $options = []){
        self::log(self::LEVEL_ERROR, $message, $data, $options);
    }

    /**
     * Writes a critical entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function critical($message, $data = '', $options = []){
        self::log(self::LEVEL_CRITICAL, $message, $data, $options);
    }

    /**
     * Writes an emergency entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function emergency($message, $data = '', $options = []){
        self::log(self::LEVEL_EMERGENCY, $message, $data, $options);
    }

    /**
     * Writes an alert entry
     * @param $message
     * @param $data
     * @param array $options ([user, module, app, file, line])
     */
    public static function alert($message, $data = '', $options = []){
        self::log(self::LEVEL_ALERT, $message, $data, $options);
    }

    /**
     * Retrieves mysql log entries
     * @param int $from_id starting entry ID (default -1)
     * @return array
     * @throws Exception
     */
    public static function get_mysql_logs($from_id = -1){
        if(self::get_config('connectors', 'mysql')){

            $db = self::db();
            try{
                $db_log_table = self::DB_LOG_TABLE;
				/** @noinspection SqlResolve */
				$query = "SELECT * FROM $db_log_table WHERE id>:from_id LIMIT 200";
                $stmt = $db->prepare($query);
                $stmt->execute(compact('from_id'));
            } catch(PDOException $ex){
                throw new Exception('DefLogger: database connector error');
            }

            $logs = $stmt->fetchAll(PDO::FETCH_OBJ);
            return $logs;
        }


    }


    //</editor-fold>


}






