<?php

namespace DefStudio\Logger;


$def_logger_config = [
    'app' => 'App Name',
    'connectors' => [
        'mysql' => [
            'db_host' => 'localhost',
            'db_name' => 'database',
            'db_user' => 'user',
            'db_password' => 'secret'
        ]
    ],
    'minimum_log_level' => 'INFO',
    'log_php_errors' => true,
    'save_lines' => [
        'before' => 10,
        'after' => 10,
        'min_level' => 'ERROR'
    ],
    'context' => [
        "/path/to/loggable/files",
    ],
    'get_user_function' => function(){
        return "NO USER";
    },
    'jwt_key' => 'JWTAPIKEY',
    'jwt_iss' => 'DefLogger',
];

