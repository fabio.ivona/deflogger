<?php

namespace DefStudio\Logger;

include dirname(__FILE__)."/../Logger.php";


//generates config file
$config_template_file = dirname(__FILE__) . "/config.php";
$config_file = dirname(__FILE__) . "/../../../../../def_logger_config.php";
if(!file_exists($config_file)) {
    copy($config_template_file, $config_file);
}



if(!function_exists('DefStudio\Logger\def_php_error_handler')){
    function def_php_error_handler($errno, $errstr, $errfile, $errline){
        if(error_reporting() === 0) {
            return true;
        } else {
            Logger::error('PHP ERROR', "($errno) $errstr", [
                'user' => 'PHP',
                'file' => $errfile,
                'line' => $errline,
                'include_stack' => true
            ]);
            return false;
        }
    }

    function def_fatal_error_handler(){
        if(@is_array($e = @error_get_last())) {
            $code = isset($e['type']) ? $e['type'] : 0;
            $msg = isset($e['message']) ? $e['message'] : '';
            $file = isset($e['file']) ? $e['file'] : '';
            $line = isset($e['line']) ? $e['line'] : '';
            if($code > 0) def_php_error_handler($code, $msg, $file, $line, null);
        }
    }

    set_error_handler("DefStudio\Logger\def_php_error_handler", E_ALL);
    register_shutdown_function('DefStudio\Logger\def_fatal_error_handler');
}